Obstacle Detector
=================

This package provides a node that detects obstacles from point cloud data (tested with [TOF camera](https://www.modalai.com/products/voxl-dk-tof)). Once the obstacle is detected, a message is sent to the mission manager.

