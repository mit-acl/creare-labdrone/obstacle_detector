/**
 * @file obstacle_detector_ros.h
 * @brief Obstacle detector
 * @author Parker Lusk <plusk@mit.edu>
 * @date 13 August 2021
 */

#pragma once

#include <string>

#include <ros/ros.h>

#include <tf2_eigen/tf2_eigen.h>
#include <tf2_ros/transform_listener.h>

#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/PointStamped.h>

#include <Eigen/Geometry>

#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/crop_box.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/common/geometry.h>

#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>

namespace acl {
namespace obstacle_detector {

  using PointCloudType = pcl::PointXYZ;
  using PointCloud = pcl::PointCloud<PointCloudType>;

  class ObstacleDetectorROS
  {
  public:
    ObstacleDetectorROS(const ros::NodeHandle& nh, const ros::NodeHandle& nhp);

  private:
    ros::NodeHandle nh_, nhp_;
    ros::Publisher pub_obs_pcd_, pub_obs_detected_;
    ros::Subscriber sub_raw_pcd_;

    PointCloud::Ptr input_cloud1_;
    PointCloud::Ptr input_cloud2_;
    PointCloud::Ptr input_cloud3_;

    double area_ratio_constant_; ///< 
    double leaf_size_filter_;
    double alpha_filter_ = 1.0;
    double ratio_filtered_ = 0.0;
    double threshold_trigger_stop_ = 0.0;

    //\brief Crop box dimensions, expressed in pcd frame
    double x_min_, x_max_;
    double y_min_, y_max_;
    double z_min_, z_max_;

    Eigen::Affine3d b_T_c_; ///< camera w.r.t body transform

    std::string body_frame_tf_; ///< name of body frame tf
    std::string tof_frame_tf_; ///< name of tof frame tf

    pcl::CropBox<PointCloudType> boxFilter_;
    pcl::VoxelGrid<PointCloudType> sorFilter_;

    void init_filters();
    void init_tf();

    void cloudCB(const sensor_msgs::PointCloud2ConstPtr& msg);
  };

} // ns obstacle_detector
} // ns acl
