/**
 * @file obstacle_detector_node.cpp
 * @brief Entry point for obstacle detector node
 * @author Parker Lusk <plusk@mit.edu>
 * @date 13 August 2021
 */

#include <ros/ros.h>

#include "obstacle_detector/obstacle_detector_ros.h"

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "obstacle_detector");
  ros::NodeHandle nhtopics("");
  ros::NodeHandle nhparams("~");
  acl::obstacle_detector::ObstacleDetectorROS node(nhtopics, nhparams);
  ros::spin();
  return 0;
}