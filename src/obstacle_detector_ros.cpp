/**
 * @file obstacle_detector_ros.cpp
 * @brief Obstacle detector
 * @author Parker Lusk <plusk@mit.edu>
 * @date 13 August 2021
 */

#include "obstacle_detector/obstacle_detector_ros.h"

namespace acl {
namespace obstacle_detector {

ObstacleDetectorROS::ObstacleDetectorROS(const ros::NodeHandle& nh, const ros::NodeHandle& nhp)
: nh_(nh), nhp_(nhp)
{
  nhp_.param<double>("cropbox/x_min", x_min_, -0.35);
  nhp_.param<double>("cropbox/x_max", x_max_,  0.35);
  nhp_.param<double>("cropbox/y_min", y_min_, -0.35);
  nhp_.param<double>("cropbox/y_max", y_max_,  0.35);
  nhp_.param<double>("cropbox/z_min", z_min_,  0.60);
  nhp_.param<double>("cropbox/z_max", z_max_, 30.00);
  nhp_.param<double>("voxel/leaf_size", leaf_size_filter_, 0.05);
  nhp_.param<double>("ratio/alpha", alpha_filter_, 0.8);
  nhp_.param<double>("ratio/threshold", threshold_trigger_stop_, 0.4);
  nhp_.param<std::string>("body_frame_tf", body_frame_tf_, "body");
  nhp_.param<std::string>("tof_frame_tf", tof_frame_tf_, "body");

  init_filters();
  init_tf();

  pub_obs_pcd_ = nh_.advertise<PointCloud>("obstacle_points", 1);
  pub_obs_detected_ = nh_.advertise<geometry_msgs::PointStamped>("obstacle", 1);
  sub_raw_pcd_ = nh_.subscribe("raw_points", 1, &ObstacleDetectorROS::cloudCB, this);
}

// ----------------------------------------------------------------------------

void ObstacleDetectorROS::init_filters()
{
  // instantiate point clouds to be used for filtering
  input_cloud1_ = PointCloud::Ptr(new PointCloud);
  input_cloud2_ = PointCloud::Ptr(new PointCloud);
  input_cloud3_ = PointCloud::Ptr(new PointCloud);

  // crop the raw point cloud, discard data outside of this box (in pcd frame)
  boxFilter_.setMin(Eigen::Vector4f(x_min_, y_min_, z_min_, 1.0));
  boxFilter_.setMax(Eigen::Vector4f(x_max_, y_max_, z_max_, 1.0));

  sorFilter_.setLeafSize(leaf_size_filter_, leaf_size_filter_, leaf_size_filter_);

  // Filtered points are classified as an obstacle once those points take up
  // a significant (based on user threshold) portion of the crop box.
  const double total_area = (x_max_ - x_min_) * (y_max_ - y_min_);
  area_ratio_constant_ = (leaf_size_filter_ * leaf_size_filter_) / total_area;
}

// ----------------------------------------------------------------------------

void ObstacleDetectorROS::init_tf()
{
  tf2_ros::Buffer tf_buffer;
  tf2_ros::TransformListener tfListener(tf_buffer);
  while (true) {
    geometry_msgs::TransformStamped transform_stamped;
    try {
      transform_stamped = tf_buffer.lookupTransform(body_frame_tf_,
                                                    tof_frame_tf_,
                                                    ros::Time(0), // just get latest
                                                    ros::Duration(0.5));
      b_T_c_ = tf2::transformToEigen(transform_stamped); // tof w.r.t body
      break;
    } catch (tf2::TransformException& ex) {
      ROS_WARN_THROTTLE(1.0, "Trying to find transform of %s wrt %s",
                              tof_frame_tf_.c_str(), body_frame_tf_.c_str());
    }
  }
  ROS_INFO("Found transform of %s wrt %s",
                              tof_frame_tf_.c_str(), body_frame_tf_.c_str());
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void ObstacleDetectorROS::cloudCB(const sensor_msgs::PointCloud2ConstPtr& msg)
{
  // Convert to PCL
  pcl::fromROSMsg(*msg, *input_cloud1_);

  // Box filter
  boxFilter_.setInputCloud(input_cloud1_);
  boxFilter_.filter(*input_cloud2_);

  // Voxel grid filter
  sorFilter_.setInputCloud(input_cloud2_);
  sorFilter_.filter(*input_cloud3_);

  // Compute the ratio between the xy area of the box used for cropping and the area with voxels in the xy plane
  const double ratio = input_cloud3_->points.size() * area_ratio_constant_;

  // LPF the ratio
  ratio_filtered_ = alpha_filter_ * ratio + (1 - alpha_filter_) * ratio_filtered_;

  if (ratio_filtered_ > threshold_trigger_stop_) {

    // Retrieve the point closest and furthest from vehicle
    PointCloudType minPt, maxPt;
    pcl::getMinMax3D(*input_cloud3_, minPt, maxPt);

    // Obstacle = closest point to the camera
    const Eigen::Vector3d c_Obs(minPt.x, minPt.y, minPt.z);  // Obstacle expressed in camera frame
    const Eigen::Vector3d b_Obs = b_T_c_ * c_Obs;            // Obstacle expressed in body frame

    geometry_msgs::PointStamped msg;
    msg.header.stamp = ros::Time::now();
    msg.header.frame_id = body_frame_tf_;
    msg.point.x = b_Obs.x();
    msg.point.y = b_Obs.y();
    msg.point.z = b_Obs.z();
    pub_obs_detected_.publish(msg);
  }

  // Publish filtered point cloud
  sensor_msgs::PointCloud2 obs_pcd_msg;
  pcl::toROSMsg(*input_cloud3_, obs_pcd_msg);
  obs_pcd_msg.header = msg->header;
  pub_obs_pcd_.publish(obs_pcd_msg);
}

} // ns obstacle_detector
} // ns acl